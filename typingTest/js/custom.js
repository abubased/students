/*************************
 * using jquery & ajax   *
 ************************/
// Shorthand for $( document ).ready()
$(function() {
    //First time these element will be hidden
    $("#btn,#message,#userString,#greeting").hide();
    //when we click on the submit button
    $("#btn1").click(()=>{
        let name = $("#userName").val();
        if (name.length != 0) {
            $("#getUserName").hide();
            let userName = name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
            $("#user").text(userName).css("font-weight","bold");
            $("#greeting,#userString,#btn").show();
            $(".container").css({"justify-content" : "flex-start","margin-top":"20px"});
        }
    });

    let startTime;
    $("#btn").click(()=>{
        //check button text "Start" or not
        if ($("#btn").text() == "Start") {
            //if we click on "Start" than this will replaced by "Done"
            $("#btn").text("Done");
            //Trun focus on in our Text area
            $("#userString").focus().val("");
            //Get a random message from a external file
            $.ajax({
                url: "file/keywords.json",
                success: function (data) {
                    var randomNumber = Math.floor(Math.random()*data.length);
                    $("#message").html(data[randomNumber]);
                }
            });
            //we will appearing our message now
            $("#message").show();
            //store the start time when we are start typing
            startTime = new Date($.now());
        } 
        //if the text of our button is not "Start" 
        else{
            //if we click on "Done" than this will replaced by "Start"
            $("#btn").text("Start");
            let message = $("#message").text();
            let userString = $("#userString").val();
            //Calculate how many of our typed words are correct
            let correctWords = compareWords(userString,message);
            //if user typed at least one correct word
            if (correctWords>1) {
                //store the endTime when we are click on done
                let endTime = new Date($.now());
                //Calculate the total time that we spend and store that
                let spendTime = (endTime-startTime)/1000;
                //Calculate how many words we are typed
                let totalWords = userString.split(" ").length;
                //Calculate typeing speed
                let speed = typingSpeed(spendTime,totalWords);
                //Change the message to result
                showResult(speed,correctWords);
            }
            $("#userString").blur();
            $("#userString").val("");
            
        }
        
    }).toggleClass("btnStyle");
    //Calculate typeing speed
    function typingSpeed(spendTime,totalWords) {
        return  (Math.round((totalWords/spendTime)*60));
    }
    //Calculate how many of our typed words are correct
    function compareWords(stringOne,stringTwo) {
        let numberOfWordsOne = stringOne.split(" ");
        let numberOfWordsTwo = stringTwo.split(" ");
        let correctWords = 0;
        numberOfWordsTwo.forEach((element,index) => {
            if (element == numberOfWordsOne[index]) {
                correctWords++;
            }
        });
        return correctWords;
    }
    function showResult(speed,correctWords) {
        //Calculate how many of our typed words are InCorrect
        let inCorrectWords = $("#message").text().split(" ").length-correctWords;
        $("#message").text("You can type "+ speed +" words per minute. and you are type "+ correctWords + " correct words & " + inCorrectWords + " wrong words.")
    }
});
